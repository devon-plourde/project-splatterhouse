﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Rope : MonoBehaviour
{
    public Transform Head;
    public Transform RopeLink;

    public float throwDistance;
    public float throwTime;

    [HideInInspector] public bool deployed;

    Rigidbody2D rigid_head;

    List<Transform> RopeLinks = new List<Transform>();
    bool Grappeled = false;
    float angle; //In Radians
    HingeJoint2D joint;

    //[HideInInspector] public bool deployed;

    //// Start is called before the first frame update
    //void Start()
    //{
        
    //}

    //// Update is called once per frame
    //void Update()
    //{

    //}

    public void ExtendRope(Vector2 position)
    {
        var lastLink = RopeLinks.Last();
        var newLink = Instantiate(RopeLink, position, Quaternion.identity, transform);
        newLink.transform.eulerAngles = new Vector3(0,0, angle / (Mathf.PI/180f));

        var lastLinkSize = lastLink.GetComponent<BoxCollider2D>().size * lastLink.transform.lossyScale;
        var newLinkSize = newLink.GetComponent<BoxCollider2D>().size * newLink.transform.lossyScale;

        var linkJoint = newLink.GetComponent<HingeJoint2D>();
        linkJoint.connectedBody = lastLink.GetComponent<Rigidbody2D>();
        linkJoint.connectedAnchor = new Vector2(0, -lastLinkSize.y);
        linkJoint.anchor = new Vector2(0, newLinkSize.y / 2);

        RopeLinks.Add(newLink);
    }

    public void ShootRope(float angleZ)
    {
        deployed = true;
        RopeLinks.Add(Head);
        rigid_head = Head.GetComponent<Rigidbody2D>();
        joint = Head.GetComponent<HingeJoint2D>();
        angle = angleZ;

        joint.enabled = false;
        Head.transform.eulerAngles = new Vector3(0, 0, angle / (Mathf.PI / 180f));
        StartCoroutine(ShootingRope());
    }

    IEnumerator ShootingRope()
    {
        var originalPosition = transform.position;
        while (!Grappeled)
        {
            Vector2 movement = new Vector2(
                Mathf.Cos(angle),
                Mathf.Sin(angle)) * throwDistance;
            Vector2 newPosition = new Vector2(rigid_head.transform.position.x + movement.x, rigid_head.transform.position.y + movement.y);
            rigid_head.MovePosition(newPosition);

            ExtendRope(originalPosition);

            yield return new WaitForSeconds(throwTime);
        }
        for(int i = 0; i < RopeLinks.Count; ++i)
        {
            var linkBody = RopeLinks[i].GetComponent<Rigidbody2D>();
            linkBody.velocity = new Vector2(0, 0);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Grappeled = true;
        joint.enabled = true;
    }
}
