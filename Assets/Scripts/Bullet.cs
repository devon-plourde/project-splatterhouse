﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float bulletSpeed;
    public Transform HitWallEffect;

    Rigidbody2D rigid;

    private void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        float degPerRad = Mathf.PI / 180f;
        Vector2 movement = new Vector2(
                Mathf.Cos(transform.eulerAngles.z * degPerRad),
                Mathf.Sin(transform.eulerAngles.z * degPerRad)) * bulletSpeed;
        rigid.MovePosition(new Vector2(transform.position.x + movement.x, transform.position.y + movement.y));
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Spawn an impact effect based on what was hit.
        if (HitWallEffect != null)
        {
            var point = collision.GetContact(0).point;
            var normal = collision.GetContact(0).normal;
            var angle = Mathf.Atan2(-normal.y, -normal.x) / (Mathf.PI / 180f);
            Instantiate(HitWallEffect, point, Quaternion.Euler(0, 0, angle));
        }
        Destroy(gameObject);
    }
}
