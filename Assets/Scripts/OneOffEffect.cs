﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneOffEffect : MonoBehaviour
{
    Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        StartCoroutine(Play());    
    }

    IEnumerator Play()
    {
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length);
        Destroy(gameObject);
    }
}
