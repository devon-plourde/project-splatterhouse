﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("References")]
    public Camera MainCam;
    public Transform Reticule;
    public Transform Hand;
    public Transform Rope;
    public Transform Belt;


    [Header("Settings")]
    public float movementSpeed;
    public float MaxSpeed;
    public float jumpForce;
    public float handDistance;

    Rigidbody2D rigid;
    FixedJoint2D beltJoint;
    bool lockCam = true;
    bool jumped = false;

    Rope deployedRope;
    bool touchingRope = false;
    Transform ropeJoint;

    // Start is called before the first frame update
    void Start()
    {
        beltJoint = GetComponent<FixedJoint2D>();
        rigid = GetComponent<Rigidbody2D>();
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Confined;
    }

    // Update is called once per frame
    void Update()
    {
        lockCam = !Input.GetMouseButton(1);
        Vector2 mousePosition = MainCam.ScreenToWorldPoint(Input.mousePosition);
        Reticule.transform.position = mousePosition;
        if (lockCam)
        {
            MainCam.transform.position = new Vector3(transform.position.x, transform.position.y, MainCam.transform.position.z);
        }
        else
        {
            Vector2 diff = new Vector2(mousePosition.x - transform.position.x, mousePosition.y - transform.position.y);
            MainCam.transform.position = new Vector3(transform.position.x + (diff.x / 2), transform.position.y + (diff.y / 2), MainCam.transform.position.z);
        }

        var degPerRad = Mathf.PI / 180f;
        var angle = Mathf.Atan2(mousePosition.y - transform.position.y, mousePosition.x - transform.position.x);
        Hand.localPosition = new Vector2(
                Mathf.Cos(angle),
                Mathf.Sin(angle)) * handDistance;
        Hand.eulerAngles = new Vector3(0, 0, angle / degPerRad);


        if (Input.GetMouseButton(0))
        {
            UseInHand();
        }

        if (ConnectedToRope())
        {
            float movement = -Input.GetAxis("Horizontal") * movementSpeed;
            var ropeAngle = Mathf.Atan2(beltJoint.connectedBody.transform.localPosition.y, beltJoint.connectedBody.transform.localPosition.x);
            rigid.velocity = new Vector2(Mathf.Cos(ropeAngle), Mathf.Sin(ropeAngle)) * movement;
            Debug.Log(rigid.velocity.normalized);
        }
        else
        {
            float velX = Input.GetAxis("Horizontal") * movementSpeed;
            if (velX > MaxSpeed)
                velX = MaxSpeed;
            else if (velX < -MaxSpeed)
                velX = -MaxSpeed;
            rigid.velocity = new Vector2(velX, rigid.velocity.y);
        }

        if (Input.GetKeyDown("space"))
        {
            if (ConnectedToRope())
            {
                DetachFromRope();
            }
            if (!jumped)
            {
                jumped = true;
                Vector2 jump = new Vector2(0, jumpForce);
                rigid.AddForce(jump);
            }
        }

        if (Input.GetKeyDown("left shift"))
        {
            //This should probably be the hand's position.
            var r = Instantiate(Rope, Belt.position, Quaternion.identity);
            deployedRope = r.GetComponent<Rope>();
            deployedRope.ShootRope(angle);
        }

        if(Input.GetAxis("Vertical") != 0 && touchingRope && !ConnectedToRope())
        {
            AttachToRope();
        }
    }

    void UseInHand()
    {
        var item = Hand.GetComponentInChildren<HeldItem>();
        if(item != null)
            item.Use();
    }

    bool ConnectedToRope()
    {
        if (beltJoint.connectedBody != null && beltJoint.connectedBody.gameObject.tag == "Rope")
            return true;
        else
            return false;
    }

    void AttachToRope()
    {
        beltJoint.connectedBody = ropeJoint.GetComponent<Rigidbody2D>();
        beltJoint.anchor = Belt.localPosition;
        beltJoint.connectedAnchor = new Vector2(0,0);
        beltJoint.enabled = true;
        //transform.eulerAngles = new Vector3(0, 0, 30);

        jumped = false;
    }

    void DetachFromRope()
    {
        beltJoint.connectedBody = null;
        beltJoint.enabled = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Geometry")
            jumped = false;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Rope")
        {
            ropeJoint = collision.transform;
            touchingRope = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Rope")
        {
            ropeJoint = null;
            touchingRope = false;
        }
    }
}
