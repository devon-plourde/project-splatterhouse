﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : HeldItem
{
    public Transform Bullet;
    public Transform Muzzle;
    [Tooltip("Number of times a bullet is fired per second.")]
    public float FireRate;

    bool _cycling = false;

    public override void Use()
    {
        if (!_cycling)
            StartCoroutine(Fire());
    }

    IEnumerator Fire()
    {
        _cycling = true;
        var b = Instantiate(Bullet, Muzzle.position, Muzzle.rotation);
        yield return new WaitForSeconds(1 / FireRate);
        _cycling = false;
    }
}
